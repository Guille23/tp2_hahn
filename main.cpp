#include "stdio.h"

float polinomio(float a, float b, float c, float x);

#define CANT_VALORES 5 

int main(void){

	float valores_a[CANT_VALORES] = { 1.0	, 2.3	, -0.4	, 0.1	, -5.4 	};
	float valores_b[CANT_VALORES] = { 3.5	, -1.0	, 2.3	, 4		, 0 	};
	float valores_c[CANT_VALORES] = { -5.2	, 2.1	, -1.3	, 1.2	, 2.3 	};
	float valores_x[CANT_VALORES] = { 1.3	, 0.5	, 2.6	, -3.2	, 1.5 	};
	float resultados[CANT_VALORES] = { -1, -1, -1, -1, -1 };
	
	for(int i=0;i<CANT_VALORES;i++){
	    resultados[i] = polinomio(valores_a[i], valores_b[i], valores_c[i], valores_x[i]);
	    //printf("%f\n", resultados[i]);
	}
	return 0 ;
}

float polinomio(float a, float b, float c, float x)
{
    return a * x * x + b * x + c;
}
